import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";

export const useNavigationBottonManagement = defineStore(
  "navigationBottonManagement",
  () => {
    const value = ref(0);
    const colors = computed(() => {
      console.log(colors.value);
      switch (value.value) {
        case 0:
          return "blue-grey";
        case 1:
          return "teal";
        case 2:
          return "yellow";
        case 3:
          return "indigo";
        default:
          return "blue-grey";
      }
    });
    const badgeTrolley = ref(0);
    const badgeOrder = ref(0);
    return { value, colors, badgeTrolley, badgeOrder };
  }
);
