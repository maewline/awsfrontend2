import { defineStore } from "pinia";
import { ref, watch } from "vue";

export const usecashPopupStore = defineStore("popupCash", () => {
  const popupCash = ref(false);
  const dialog = ref(false);

  const editedCash = ref({
    cash: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    // console.log(newDialog);
    if (!newDialog) {
      editedCash.value = {
        cash: 0,
      };
    }
  });
  return { dialog, editedCash };
});
