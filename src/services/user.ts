import type User from "@/types/User";
import http from "./axios";
function getUsers() {
  return http.get("/users");
}
function userLogin(login: string, password: string) {
  return http.post("/users/login", { login, password });
}

function saveUser(user: User & { files: File[] }) {
  const formData = new FormData();
  formData.append("login", user.login);
  formData.append("name", user.name);
  formData.append("password", user.password);
  formData.append("position", user.position);
  formData.append("file", user.files[0]);
  return http.post("/users", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}
function getByUsername(username: string) {
  return http.get(`/users/${username}`);
}

function updateUser(id: number, user: User & { files: File[] }) {
  const formData = new FormData();
  formData.append("login", user.login);
  formData.append("name", user.name);
  formData.append("password", user.password);
  formData.append("position", user.position);
  console.log(user.files);
  if (user.files) {
    formData.append("file", user.files[0]);
  }

  return http.patch(`/users/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

// function updateUser(id: number, user: User) {
//   return http.patch(`/users/${id}`, user);
// }

function daleteUser(id: number) {
  return http.delete(`/users/${id}`);
}

function getUserByID(id: number) {
  return http.get(`/users/${id}`);
}

function getEmpByUserID(id: number) {
  return http.get(`/users/${id}`);
}

export default {
  getUsers,
  saveUser,
  updateUser,
  daleteUser,
  getUserByID,
  getByUsername,
  userLogin,
  getEmpByUserID,
};
