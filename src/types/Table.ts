// import type Order from "./Order";

import type Order from "./order";

export default interface Table {
  id?: number;
  name?: string;
  amount?: number;
  status?: string;
  order?: Order[];
}
