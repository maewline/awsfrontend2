import type OrderItem from "./OrderItem";

export default interface Food {
  id?: number;
  name: string;
  type: string;
  status: boolean;
  img?: string;
  price: number;
  info: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  orderItems?: OrderItem[];
  // option: Option[];
}

// type Option = {
//   name: string;
//   price: number;
// };
