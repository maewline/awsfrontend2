import type Employee from "./employee";

export default interface CheckInOut {
  id?: number;
  timestart?: Date;
  timeend?: Date;
  hour?: number;
  status?: string;
  employee?: Employee;
}
